#include <iostream>

using namespace std;

unsigned long  int euler(unsigned  long int n)
{
    unsigned long  int ret = 1;

    for(int i = 2; i * i <= n; ++i)
    {
        unsigned  long int k = 1;

        while(n % i == 0)
        {
            k *= i;
            n /= i;
        }
        if((k /= i) >= 1)
        {
            ret *= k * (i - 1);
        }
    }
    return --n ? n * ret : ret;
}
int main()
{
    cout<< euler(78098325436);
    return 0;
}
